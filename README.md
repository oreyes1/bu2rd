Thi is a tiny cURL script which shows how to programmatically log-in into Bitbucket and automatically submit files to the download area of any repository you own. Useful for continuous integration and other unattended tasks.

### Usage
	bu2rd.sh <user> <password> /<acct>/<repo>/downloads <file to upload>
